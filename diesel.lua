local info = debug.getinfo(1,'S');
local src = info.source

assert(src:sub(1,1) == "@")
src = src:sub(2)

-- Use UNIX path seperators, even while on Windows
src = src:gsub("\\", "/")

-- TODO verify src exists
local file_suffix = "/diesel.lua"
assert(src:sub(-file_suffix:len()) == file_suffix)
src = src:sub(1, -4 -1)

ide:Print("Loading DieselX plugin from " .. src)

local f, err = loadfile(src .. "/main.lua")

if not f then error(err) end

return f({
	path = src
})
