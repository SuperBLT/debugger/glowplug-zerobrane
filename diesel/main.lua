local args = ...
local lfs = require("lfs")

local glownat

local default_assets_dir = {
	Windows = "C:\\Users\\ZNix\\Static\\steamcmd\\steamapps\\common\\PAYDAY 2\\assets",
	Unix = nil,
}

local assets_dir = nil -- set in onRegister

local diesel_static = _G.__diesel_static

-- Use locally-built debug versions of the native libraries
local using_debug_natives = false

local function lazy_load(fn)
	local func

	return function(...)
		if not func then
			func = dofile(args.path .. "/" .. fn)
		end

		return func(...)
	end
end

local do_hook_setup = lazy_load("hooks.lua")
local create_bundle_tree = lazy_load("bundle_tree.lua")

-- Strictly one-time setup
function glownat_setup()
	if diesel_static then
		glownat = diesel_static.glownat
		return
	end

	diesel_static = {}
	_G.__diesel_static = diesel_static

	local w32 = ide.osname == "Windows"
	local loadlib = package.loadlib

	local function load_lib(build_name, path, name, binary_dir)
		local pre = w32 and "" or "lib"
		path = pre .. path

		if using_debug_natives then
			if w32 then
				path = ("%s/build/Debug/%s"):format(build_name, path)
			else
				path = ("%s/build/%s"):format(build_name, path)
			end
		else
			path = (binary_dir or "natives/") .. path
		end

		local ext = w32 and "dll" or "so"

		local native_path = ("%s/%s.%s"):format(args.path, path, ext)
		local glownat_f, err = loadlib(native_path, name)

		if not glownat_f then
			error("Error loading '" .. native_path .. "' (Diesel native library): " .. tostring(err))
			return
		end

		return glownat_f({
			path = args.path,
		})
	end

	glownat = load_lib("lua-glownat", "glownat", "luaopen_glownat")
	diesel_static.glownat = glownat
	loadlib = glownat.adv_load.loadlib or loadlib

	diesel_static.libsd = load_lib("lua-scriptdata", "scriptdata", "luaopen_scriptdata")

	-- [Windows only] Preload the python DLL
	-- Since CPython is installed in it's own directory, Lython otherwise wouldn't
	--  be able to find it, and would fall back to the user's manually installed copy.
	--
	-- If the user has not installed Python 3.7 (or whatever version Lython is compiled
	--  against), Lython would fail to load - hence the embedded copy.
	local py_dll_ref
	if w32 then
		local ref, err = glownat.adv_load.preload(args.path .. "/python/python37.dll", {
			w32_alt_load_path = true,
		})
		py_dll_ref = ref
		if not py_dll_ref then
			err = err:gsub("%s+$", "") -- Strip trailing whitespace, esp. newlines
			local str = "Cannot load embedded python DLL, will attempt to use system copy (error: %s)"
			ide:Print(str:format(err))
		end
	end

	diesel_static.python = load_lib("lython", "lython", "luaopen_pyinterop")

	if py_dll_ref then
		glownat.adv_load.preload_release(py_dll_ref)
	end

	local hl = socket.gettime()
	local fi = args.path .. "/hashlist"
	glownat.hashlist.loadfile(fi)

	local start = socket.gettime()
	print("\nStart load " .. tostring((start - hl) * 1000))
	glownat.bundle.load_db(MergeFullPath(assets_dir, "bundle_db.blb"))
	glownat.bundle.load_all_header(MergeFullPath(assets_dir, "all_h.bundle"))
	local t1 = socket.gettime()
	print("load 1 " .. tostring((t1 - start) * 1000))

	for file in lfs.dir(assets_dir) do
		-- Note: check for anything starting with all_, which catches the legacy bundle things if they
		--  weren't deleted or were recreated with the tool before Bundle Modder was updated to support them
		if file:sub(-9) == "_h.bundle" and file:sub(1,4) ~= "all_" then
			local path = MergeFullPath(assets_dir, file)
			glownat.bundle.load_header(path)
		end
	end

	local t2 = socket.gettime()
	print("load 2 " .. tostring((t2 - t1) * 1000))
	glownat.bundle.update_tree()
	local tend = socket.gettime()
	print("End load " .. tostring((tend - t2) * 1000))

	-- Do some modifications not cleanly possible with the standard API
	do_hook_setup()
end

local plugin = {
	name = "ZB-Diesel",
	description = "DieselX engine intergration for ZeroBrane",
	author = "Campbell Suter",
	version = 0.1,
}

assert(loadfile(args.path .. "/extract.lua"))(plugin, args)

function plugin:_get_file_by_name(path)
	local dir = glownat.tree.root()

	local parts = {}
	for part in path:gmatch("[^/]+") do
		table.insert(parts, part)
	end

	for i=1, #parts-1 do
		local part = parts[i]
		local dirs = glownat.tree.list_dirs(dir)
		dir = assert(dirs[part], "missing directory " .. tostring(part))
	end

	local filename = parts[#parts]

	local files = glownat.tree.list_files(dir)
	for _, file in ipairs(files) do
		if file.name .. "." .. file.type == filename then
			return file
		end
	end

	error("Cannot find file " .. tostring(path))
end

function plugin:onRegister()
	assets_dir = self:GetConfig().assets_dir or default_assets_dir[ide.osname]
	local ad_exists = assets_dir and wx.wxDirExists(assets_dir)
	if not ad_exists then
		if assets_dir and self:GetConfig().assets_dir then
			ide:Print("Glowplug: The specified assets directory does not exist: " .. assets_dir)
		end
		ide:Print("Glowplug: Cannot find PAYDAY 2 assets dir! Please set it with the following in your preferences:")
		ide:Print("diesel = { assets_dir = \"path_to_my_assets_dir\" }")
		return
	end

	glownat_setup()
	diesel_static.plugin = plugin

	-- Used by extract
	self._extracted_files = {}
	self._assets_dir = assets_dir

	self._bundle_tree = create_bundle_tree({
		open_item = function(info)
			local temp_fn = self:extract_file(info)

			-- Open it in the IDE
			local editor = LoadFile(temp_fn, nil, true)

			if not editor then
				-- File failed to open, not a lot we can do
				return
			end

			self:_update_editor_file(editor, info)
		end,
	})

	ide:GetProjectNotebook():AddPage(self._bundle_tree:tree(), TR("Bundles"), false)
end

function plugin:_update_editor_file(editor, info)
	editor:SetReadOnly(true)
	local doc = ide:GetDocument(editor)
	doc:SetTabText(info.name .. "." .. info.type)
end

function plugin:onUnRegister()
	local nb = ide:GetProjectNotebook()
	local index = nb:GetPageIndex(self._projtree)
	if index ~= wx.wxNOT_FOUND then
		nb:RemovePage(index)
	end
end

function plugin:onAppShutdown()
	glownat.shutdown()
end

return plugin
