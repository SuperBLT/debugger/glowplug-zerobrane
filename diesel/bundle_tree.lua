local diesel_static = _G.__diesel_static
local glownat = diesel_static.glownat

local bundle_tree = {}

local imglist = ide:CreateImageList("PROJECT",
    "FOLDER", "FOLDER-MAPPED", "FILE-NORMAL", "FILE-NORMAL-START",
    -- "file known" needs to be the last one as dynamic types can be added after it
    "FILE-KNOWN")

local image = { -- the order of ids has to match the order in the ImageList
	DIRECTORY = 0, DIRECTORYMAPPED = 1, FILEOTHER = 2, FILEOTHERSTART = 3,
	FILEKNOWN = 4,
}

local function sorted_pairs(t)
	local function sorted_next(state)
		local k = state.keys[state.i]
		state.i = state.i + 1
		return k, k and state.t[k]
	end
	local state = { t=t, keys={}, i=1, }
	for k, _ in pairs(t) do
		table.insert(state.keys, k)
	end
	table.sort(state.keys)
	return sorted_next, state, nil
end

-- Bundle tree view system
--
-- For performance reasons, this doesn't load all 300,000 items (plus directories) from
--  the bundles into the tree control - that greatly decreases both RAM usage (glownat carefully
--  packs stuff into structs to minimise RAM usage, wxWidgets doesn't) and loading time.
--
-- Rather, each directory contains a single 'stub' item inside it (to make it expandable. When
--  that directory is opened, it's contents is swapped out with the real contents. When it
--  is collapsed, it goes back to the stub node.

function bundle_tree:init(conf)
	assert(type(conf) == "table", "config table missing")
	assert(type(conf.open_item) == "function", "config.open_item callback missing")

	-- project
	self._tree = ide:CreateTreeCtrl(ide.frame, wx.wxID_ANY,
		wx.wxDefaultPosition, wx.wxDefaultSize,
		wx.wxTR_HAS_BUTTONS + wx.wxTR_MULTIPLE + wx.wxTR_LINES_AT_ROOT + wx.wxTR_EDIT_LABELS + wx.wxNO_BORDER + wx.wxTR_HIDE_ROOT)
	local tree = self._tree

	tree:AssignImageList(imglist)
	tree:SetFont(ide.font.tree)

	self._file_info = {}
	self._collapsed_unloaded = {}

	local root = glownat.tree.root()
	local dirs = glownat.tree.list_dirs(root)

	-- Hidden, and only it's children displayed as 'root' nodes
	-- This is done via wxTR_HIDE_ROOT, in the flags above
	local root_node = tree:AddRoot("Root")

	local bundled_files = {}
	local unknown_files = {}
	for _, f in ipairs(glownat.tree.list_files(root)) do
		if not f.unknown_name then
			table.insert(bundled_files, f)
		else
			table.insert(unknown_files, f)
		end
	end

	local function reject() return false end

	local core_lib = glownat.tree.list_dirs(dirs.core).lib
	local function lua_dir_filter(dir)
		if dir == dirs.lib then
			return true
		elseif dir ~= dirs.core then
			return false
		end

		return true, {
			dir_filter = function(dir)
				return dir == core_lib
			end,
			file_filter = reject,
		}
	end

	local function lua_file_filter(file)
		return file.type == "lua"
	end

	local function bundle_filter(dir)
		if dir == dirs.lib then
			return false
		elseif dir ~= dirs.core then
			return true
		end

		return true, {
			dir_filter = function(dir)
				return dir ~= core_lib
			end,
		}
	end

	local function unknown_file_filter(file)
		return not lua_file_filter(file)
	end

	local lua_node = self:_create_closed_node(root_node, "Lua", {
		dir = root,
		dir_filter = lua_dir_filter,
		file_filter = lua_file_filter,
	})

	local bundles_node = self:_create_closed_node(root_node, "Bundles", {
		dir = root,
		dir_filter = bundle_filter,
		file_filter = reject,
	})

	local unknown_node = self:_create_closed_node(root_node, "Unknown", {
		dir = root,
		dir_filter = reject,
		file_filter = unknown_file_filter,
	})

	tree:Connect(wx.wxEVT_COMMAND_TREE_ITEM_EXPANDING, function(event)
		self:_node_expanding(event)
	end)

	tree:Connect(wx.wxEVT_COMMAND_TREE_ITEM_COLLAPSED, function(event)
		self:_node_collapsing(event)
	end)

	tree:Connect(wx.wxEVT_COMMAND_TREE_ITEM_ACTIVATED, function(event)
		local item = event:GetItem()
		local img = tree:GetItemImage(item)

		if img == image.DIRECTORY then
			tree:Toggle(item)
			return
		end

		local info = self._file_info[item:GetValue()]

		conf.open_item(info)
	end)

	tree:Expand(lua_node)
	tree:Expand(bundles_node)

	local function veto(event)
		event:Veto()
	end

	tree:Connect(wx.wxEVT_COMMAND_TREE_BEGIN_DRAG, veto)
	tree:Connect(wx.wxEVT_COMMAND_TREE_BEGIN_RDRAG, veto)
	tree:Connect(wx.wxEVT_COMMAND_TREE_BEGIN_LABEL_EDIT, veto)
	tree:Connect(wx.wxEVT_COMMAND_TREE_DELETE_ITEM, veto)
end

function bundle_tree:tree()
	return self._tree
end

function bundle_tree:_node_expanding(event)
	local item = event:GetItem()

	local info = self._collapsed_unloaded[item:GetValue()]
	assert(info, "Missing bundle directory information!")

	-- Clear out the stub item
	self._tree:DeleteChildren(item)

	local dirs = glownat.tree.list_dirs(info.dir)
	local dir_filter = info.dir_filter

	for name, dir in sorted_pairs(dirs) do
		local visible, params

		if dir_filter then
			visible, params = dir_filter(dir)
		else
			visible = true
		end

		if visible then
			local info = {
				dir = dir,
			}

			if params then
				for k, v in pairs(params) do
					info[k] = v
				end
			end

			self:_create_closed_node(item, name, info)
		end
	end

	local files = glownat.tree.list_files(info.dir)
	local file_filter = info.file_filter

	for _, file in sorted_pairs(files) do
		if not file_filter or file_filter(file) then
			local name = file.name .. "." .. (file.lang and file.lang .. "." or "") .. file.type
			local fnode = self._tree:AppendItem(item, name, image.FILEOTHER)
			self._file_info[fnode:GetValue()] = file
		end
	end
end

function bundle_tree:_node_collapsing(event)
	local item = event:GetItem()

	-- Remove the node's children from the file info table, since they're no longer accessable
	self:_unregister_node_children(item)

	-- Delete the children themselves (they'll be regenerated when the node is expanded)
	self._tree:DeleteChildren(item)

	-- Make sure it still shows as a directory
	self:_create_stub_node(item)

	-- Debugging, to ensure there aren't any leftovers in _file_info
	-- local count = 0
	-- for _ in pairs(self._file_info) do count = count + 1 end
	-- ide:Print("Items registered: ", count)
end

function bundle_tree:_unregister_node_children(item)
	local child, cookie = self._tree:GetFirstChild(item)
	while child:IsOk() do
		self:_unregister_node_children(child) -- noop when running on a leaf node
		self._file_info[child:GetValue()] = nil
		child, cookie = self._tree:GetNextChild(item, cookie)
	end
end

function bundle_tree:_create_stub_node(node)
	self._tree:AppendItem(node, "stub item", image.DIRECTORY)
end

function bundle_tree:_create_closed_node(parent, name, info)
	local node = self._tree:AppendItem(parent, name, image.DIRECTORY)

	-- Create an item to make it show up as a directory
	self:_create_stub_node(node)

	self._collapsed_unloaded[node:GetValue()] = info

	return node
end

--------

local function create_bundle_tree(...)
	local obj = {}
	setmetatable(obj, {
		__index = bundle_tree
	})

	obj:init(...)

	return obj
end

return create_bundle_tree
