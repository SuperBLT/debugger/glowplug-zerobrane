local diesel_static = _G.__diesel_static

local function starts_with(str, part)
	return str:sub(1, part:len()) == part
end

local function do_hook_setup()
	if diesel_static.has_installed_hooks then
		return
	end
	diesel_static.has_installed_hooks = true

	local debugger = getmetatable(ide:GetDebugger()).__index

	local function remap_file(file)
		if starts_with(file, "core/lib/") or starts_with(file, "lib/") then
			local old_file = file
			file = diesel_static.plugin:extract_file(file)

			-- Get the absolute path, otherwise this chokes when run on Windows
			file = FileNormalizePath(file)

			--ide:Print(("Remapped %s to %s"):format(old_file, file))
		end
		return file
	end

	local old_ActivateDocument = debugger.ActivateDocument
	function debugger:ActivateDocument(file, line, activatehow, ...)
		local old_file = file
		local plugin = diesel_static.plugin
		file = remap_file(file)

		local info = plugin._extracted_files[file]
		local ed = old_ActivateDocument(self, file, line, activatehow, ...)

		-- If we're working on an extracted file, and the editor isn't opened in pending mode, then set it up
		if info and ed and ed ~= true then
			plugin:_update_editor_file(ed, info)
		end

		return ed
	end

	local old_handle = debugger.handle
	function debugger:handle(command, server, options, ...)
		local file, line, err = old_handle(self, command, server, options, ...)

		if err then
			return file, line, err
		end

		if type(file) ~= "string" or (type(line) ~= "string" and type(line) ~= "number") then
			return file, line, err
		end

		-- Make sure the file is loaded, so we can do the line unmapping
		local mapped_file = remap_file(file)

		if mapped_file == file then
			return file, line, err
		end

		line = assert(tonumber(line))
		--ide:Print(file, line)

		local plugin = diesel_static.plugin
		local status
		status, line = pcall(plugin.remap_line, plugin, mapped_file, line)
		local orig_line = line

		--ide:Print(file, orig_line, status, line)

		return file, line, err
	end

	local function tweakBreakpoint(file, line)
		local plugin = diesel_static.plugin
		local norm_file = FileNormalizePath(file)
		local info = plugin._extracted_files[norm_file]
		if info and info.type == "lua" then
			local mapped_line = plugin:remap_line(norm_file, line, true)
			ide:Print(norm_file, info.path, line, mapped_line, state)

			file = info.path .. "." .. info.type
			line = mapped_line
		end

		return file, line
	end

	local old_breakpoint = debugger.breakpoint
	function debugger:breakpoint(file, line, state, ...)
		file, line = tweakBreakpoint(file, line)
		return old_breakpoint(self, file, line, state, ...)
	end

	-- Unfortunately, there's not really any nice way of handling this other than just overwriting the whole function
	local old_reset_breakpoints = debugger.reSetBreakpoints
	function debugger:reSetBreakpoints(...)
		-- go over all windows and find all breakpoints
		if debugger.scratchpad then
			return old_reset_breakpoints(self, ...)
		end

		local debugger = self
		-- remove all breakpoints that may still be present from the last session
		-- this only matters for those remote clients that reload scripts
		-- without resetting their breakpoints
		debugger:handle("delallb")

		local BREAKPOINT_MARKER = StylesGetMarker("breakpoint")
		local BREAKPOINT_MARKER_VALUE = 2^BREAKPOINT_MARKER

		for _, document in pairs(ide:GetDocuments()) do
			local editor = document:GetEditor()
			local filePath = document:GetFilePath()
			local line = editor:MarkerNext(0, BREAKPOINT_MARKER_VALUE)
			while filePath and line ~= -1 do
				-- This is the point of hooking this function - remap the breakpoint
				local bfile, bline = tweakBreakpoint(filePath, line + 1)

				debugger:handle("setb " .. bfile .. " " .. bline)
				line = editor:MarkerNext(line + 1, BREAKPOINT_MARKER_VALUE)
			end
		end
	end
end

return do_hook_setup
