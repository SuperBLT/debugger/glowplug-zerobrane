local plugin, args = ...

-- List from https://github.com/Luffyyy/DieselEngineFormats/
local scriptdata_types = {
	["sequence_manager"] = true,
	["environment"] = true,
	["menu"] = true,
	["continent"] = true,
	["continents"] = true,
	["mission"] = true,
	["nav_data"] = true,
	["cover_data"] = true,
	["world"] = true,
	["world_cameras"] = true,
	["prefhud"] = true,
	["objective"] = true,
	["credits"] = true,
	["hint"] = true,
	["comment"] = true,
	["dialog"] = true,
	["dialog_index"] = true,
	["timeline"] = true,
	["action_message"] = true,
	["achievment"] = true,
	["controller_settings"] = true,
}

local already_decompiled = {}
function plugin:extract_file(info)
	local glownat = __diesel_static.glownat

	if type(info) == "string" then
		info = self:_get_file_by_name(info)
		assert(info)
	end

	glownat.tree.get_extended_info(info)

	-- See if we've already decompiled the file - if so, just open it
	if already_decompiled[info.file_id] then
		return already_decompiled[info.file_id]
	end

	local bundle_file = info.bundle_file
	if not bundle_file then
		bundle_file = self._assets_dir .. "/all_" .. tostring(info.bundle_id) .. ".bundle"
	end

	local fi = io.open(bundle_file, "rb")

	local len = info.length
	if not len then
		local fi_len = fi:seek("end")
		len = fi_len - info.offset
	end

	fi:seek("set", info.offset)

	local temp_fn = glownat.get_temp_dir(args.path) .. "/extract-" .. tostring(info.file_id) .. "." .. info.type
	temp_fn = FileNormalizePath(temp_fn)
	local out = io.open(temp_fn, "wb")

	--ide:Print("length", len)
	--ide:Print("out name", temp_fn)
	--ide:Print("bundle file", bundle_file, info.path)

	local function extractTo(outfi, transform)
		-- Display the raw file
		local count = 0
		local bs = 2^13 -- 8KiB
		while count ~= len do
			local nowcount = math.min(len - count, bs)

			local data = fi:read(nowcount)
			assert(data)

			if transform then
				local l = data:len()

				data = transform(data, count)

				assert(data)
				assert(data:len() == l)
			end

			outfi:write(data)

			count = count + data:len()
		end

		assert(count == len)
	end

	if info.type == "lua" then
		-- the decompiler will write into the output file
		out:close()
		out = nil

		-- put the extracted and decrypted file somewhere
		local temp_bin = glownat.get_temp_dir(args.path) .. "/extract-" .. tostring(info.file_id) .. ".luac"
		local bin_out = io.open(temp_bin, "wb")
		extractTo(bin_out, function(block, offset)
			-- Decrypt this block, with PD2's Lua encryption key
			return glownat.crypto.transform("asljfowk4_5u2333crj", len, offset, block)
		end)
		bin_out:close()

		local res = __diesel_static.python.run_file(args.path .. "/luajit-decompiler/main.py", "-f", temp_bin,
				"-o", temp_fn, "--line-map-output", temp_fn .. ".linemap")
		--print("res: ", res)
	elseif scriptdata_types[info.type] then
		-- Decode the file via the scriptdata-to-XML decompiler
		-- TODO option to display this as a Lua table

		local data = fi:read(len)
		local val = __diesel_static.libsd.read_xml(data)

		out:write("<!-- Decompiled from precompiled Lua ScriptData - formatted as XML -->\n")
		out:write(val)
		out:write("\n")

	else
		extractTo(out)
	end

	if out then
		out:close()
	end

	fi:close()

	already_decompiled[info.file_id] = temp_fn
	self._extracted_files[temp_fn] = info

	return temp_fn
end

function plugin:remap_line(base_fn, in_line, reverse_map)
	local remap_fn = base_fn .. ".linemap"

	local map = assert(io.open(remap_fn, "rb"))

	local function read_int(str, i)
		local a, b, c, d = str:byte(i, i+3)
		return a * 2^24 + b * 2^16 + c * 2^8 + d * 2^0
	end

	local data = map:read("*all")
	if reverse_map then
		local lines = {}
		for i = 1, data:len(), 8 do
			table.insert(lines, { read_int(data, i), read_int(data, i+4) })
		end
		table.sort(lines, function(a, b)
			return a[2] < b[2]
		end)

		local last
		for _, pair in ipairs(lines) do
			last = pair

			if in_line < pair[2] then
				break
			end
		end

		return last[1]
	else
		local from
		for i = 1, data:len(), 8 do
			from = read_int(data, i)

			--ide:Print(from)
			if in_line <= from then
				local out_line = read_int(data, i + 4)
				--ide:Print(in_line, out_line)
				return out_line
			end
		end
	end

	-- Use the last mapped line
	return from
end
